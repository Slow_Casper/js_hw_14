

const body = document.querySelector("body");
const btn = document.querySelector(".theme_btn");


btn.addEventListener("click", () => {
    body.classList.toggle("dark_theme");
    if(body.classList.contains("dark_theme")) {
        localStorage.setItem("theme", "dark_theme")
    }else{
        localStorage.setItem("theme", "ligth_theme")
    }
})

window.addEventListener("load", () => {
    let theme = localStorage.getItem("theme");
    if(theme){
        body.classList.add(theme);
    }
})
